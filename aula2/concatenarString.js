// Concatenação Básica
var fullName = "Bob Bobbington";
var age = 37;
var sentence = "Hello, my name is " + fullName + ".\n\n" +
    "I'll be " + (age + 1) + " years old next month.";
console.log(sentence);
// Template string
var fullName2 = "Bob Bobbington";
var age2 = 37;
var sentence2 = "Hello, my name is " + fullName2 + ".\n\nI'll be " + (age2 + 1) + " years old next month.";
console.log(sentence2);
