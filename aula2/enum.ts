enum Color {
    Red,
    Green,
    Blue
}
Color.Red // console.log = 0
Color.Green // console.log = 1
Color.Blue // console.log = 2

enum Color {Red2 = 1, Green2, Blue2}
let c: Color = Color.Green2;
console.log( c ); //return 2
