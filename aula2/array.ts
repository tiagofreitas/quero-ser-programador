// Element Array
let list: number[] = [1, 2, 3];
console.log( list );

// Generic Array
let list2: Array<number> = [1, 2, 3];
console.log( list2 );
