// Concatenação Básica
let fullName: string = "Bob Bobbington";
let age: number = 37;
let sentence: string = "Hello, my name is " + fullName + ".\n\n" +
    "I'll be " + (age + 1) + " years old next month.";
console.log( sentence );

// Template string
let fullName2: string = `Bob Bobbington`;
let age2: number = 37;
let sentence2: string = `Hello, my name is ${ fullName2 }.

I'll be ${ age2 + 1 } years old next month.`
console.log( sentence2 );
