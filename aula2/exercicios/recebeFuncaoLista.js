//Crie um método chamado calculadora que receberá uma lista de argumentos + função callback que fara o cálculo desejado.
var somaValores = function (parcela1, parcela2) { return (parcela1 + parcela2); };
var calculaValores = function (callBack, listaNumeros) {
    var resultado = listaNumeros[0];
    for (var i = 1; i < listaNumeros.length; i++) {
        resultado = callBack(resultado, listaNumeros[i]);
    }
    return resultado;
};
console.log("Com função Soma: ", calculaValores(somaValores, [27, 2, 4, 6, 8]));
var subtrairValores = function (parametro1, parametro2) { return (parametro1 - parametro2); };
console.log("Com função Subtrair: ", calculaValores(subtrairValores, [27, 2, 4, 6, 8]));
var multiplicaValores = function (parametro1, parametro2) { return (parametro1 * parametro2); };
console.log("Com função Multiplicar: ", calculaValores(multiplicaValores, [27, 2, 4, 6, 8]));
var divideValores = function (parametro1, parametro2) { return (parametro1 / parametro2); };
console.log("Com função Dividir: ", calculaValores(divideValores, [27, 2, 4, 6, 8]));
/*
type somaType = ( parameter1:number, parameter2:number ) => number
type calculadoraType = ( callback:somaType, parameters:number[] ) => number

let somaValores:somaType = ( parcela1:number, parcela2:number ) => (parcela1+parcela2);
let calculaValores:calculadoraType = ( callBack:somaType, listaNumeros:number[] ) => {
    let resultado = 0;
    for (var i=0; i < listaNumeros.length;i++) {
        resultado = callBack( resultado, listaNumeros[i] )
    }
    return resultado;
}

console.log( "Com função Soma: ",calculaValores(somaValores, [27,2,4,6,8]) );
 */ 
