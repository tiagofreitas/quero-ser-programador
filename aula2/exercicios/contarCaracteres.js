//Crie um método que receba uma lista de palavras e retorne um novo array com a quantidade de caracteres.
var contar = function (valor) { return valor.length; };
var contarCaracteres = function (lista) { return lista.map(contar); };
console.log(contarCaracteres(['abc', 'abcggg', 'abcf', 'abcer']));

/*
type intervaloDiasType = ( parameter1:number, parameter2:number ) => string

Crie um método chamado calculadora que receberá uma lista de argumentos + função callback que fara o cálculo desejado.

type calculadoraType = ( callback:somaType, ...parameters:number[] ) => number
*/
