var somaValores = function (parcela1, parcela2) { return (parcela1 + parcela2); };
console.log("\n1:Função controlada por tipo:", somaValores(13, 45));
var somarValores2 = function (parcela1, parcela2) { return (parcela1 + parcela2); };
console.log("\n2:Função criada a partir da interface:", somarValores2(13, 45));
// Classe com função de soma
var Somadora = /** @class */ (function () {
    function Somadora(parcela1, parcela2) {
        var _this = this;
        this.parcela1 = parcela1;
        this.parcela2 = parcela2;
        this.somar = function () { return (_this.parcela1 + _this.parcela2); };
    }
    return Somadora;
}());
var somarValores3 = new Somadora(13, 45);
console.log("\n3:Função criada a partir da classe:", somarValores3.somar());
/*
Crie um método que some dois argumentos respeitando a seguinte tipagem:

Crie um método que receba uma lista de palavras e retorne um novo array com a quantidade de caracteres.

Crie um método que retorna a quantidade de anos, meses, dias, horas, minutos, segundos, de intervalo entre duas datas.


type intervaloDiasType = ( parameter1:number, parameter2:number ) => string

Crie um método chamado calculadora que receberá uma lista de argumentos + função callback que fara o cálculo desejado.


type calculadoraType = ( callback:somaType, ...parameters:number[] ) => number
*/ 
