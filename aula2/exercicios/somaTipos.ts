// Tipo, que define os tipos de parâmetros de entrada e saída da função
type somaType = ( parameter1:number, parameter2:number ) => number

let somaValores:somaType = ( parcela1:number, parcela2:number ) => (parcela1+parcela2);

console.log( "\n1:Função controlada por tipo:",somaValores( 13,45) );

// Interface, que define os tipos de parâmetros de entrada e saída da função
interface Soma {
    (parameter1:number,parameter2:number) : number
}

let somarValores2:Soma = ( parcela1:number, parcela2:number ) => ( parcela1 + parcela2 );

console.log( "\n2:Função criada a partir da interface:",somarValores2( 13,45) );

// Classe com função de soma
class Somadora {
    constructor( private parcela1:number,private parcela2:number ) {}
    somar = () => ( this.parcela1 + this.parcela2 );
}

let somarValores3 = new Somadora( 13, 45 );

console.log( "\n3:Função criada a partir da classe:",somarValores3.somar() );

/*
Crie um método que some dois argumentos respeitando a seguinte tipagem:

Crie um método que receba uma lista de palavras e retorne um novo array com a quantidade de caracteres.

Crie um método que retorna a quantidade de anos, meses, dias, horas, minutos, segundos, de intervalo entre duas datas.


type intervaloDiasType = ( parameter1:number, parameter2:number ) => string

Crie um método chamado calculadora que receberá uma lista de argumentos + função callback que fara o cálculo desejado.


type calculadoraType = ( callback:somaType, ...parameters:number[] ) => number
*/