//Crie um método que retorna a quantidade de anos, meses, dias, horas, minutos, segundos, de intervalo entre duas datas.
var data1 = new Date('2017-01-01T00:00:00');
var data2 = new Date('2018-05-01T23:59:59');
var contarDiferencaDatas = function (dataInicio, dataFim) {
    var diferenca = dataFim.valueOf() - dataInicio.valueOf();
    var difDias = Math.floor(diferenca / (1000 * 3600 * 24));
    var difMeses = Math.floor(difDias / (30.4375));
    var difAnos = Math.floor(difDias / (365.25));
    return "A difereça é de aproximadamente " +
        difAnos +
        ((difAnos == 1) ? " ano, " : " anos, ") +
        (difMeses - (difAnos * 12)) +
        ((difMeses == 1) ? " mês e " : " meses e ") +
        Math.floor((difDias - (difAnos * 365.25))) +
        ((difDias == 1) ? " dia;" : " dias;");
};
console.log(contarDiferencaDatas(data1, data2));
