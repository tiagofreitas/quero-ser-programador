//Crie um método que receba uma lista de palavras e retorne um novo array com a quantidade de caracteres.
let contar = ( valor:string ) => valor.length;

let contarCaracteres = ( lista:string[] ) => lista.map( contar );

console.log( contarCaracteres( ['abc', 'abcggg', 'abcf', 'abcer'] ) );

//Crie um método que retorna a quantidade de anos, meses, dias, horas, minutos, segundos, de intervalo entre duas datas.
var data1:Date = new Date( '2017-01-01T00:00:00' );
var data2:Date = new Date( '2018-05-01T23:59:59' );
let contarDiferencaDatas = ( dataInicio:Date, dataFim:Date ) => {
            var diferenca:number = dataFim.valueOf() - dataInicio.valueOf();
            var difDias:number = Math.floor( diferenca / ( 1000 * 3600 * 24 ) );
            var difMeses:number = Math.floor( difDias / ( 30.4375 ) );
            var difAnos:number = Math.floor( difDias / ( 365.25 ) );
            return "A difereça é de aproximadamente " +
                difAnos +
                ( ( difAnos == 1 ) ? " ano, " : " anos, " ) +
                ( difMeses - ( difAnos * 12 ) ) +
                ( ( difMeses == 1 ) ? " mês, " : " meses, " ) +
                Math.floor( ( difDias - ( difAnos * 365.25 ) ) ) +
                ( ( difDias == 1 ) ? " dia;" : " dias;" )

}

console.log( contarDiferencaDatas( data1, data2 ) );

/*
type intervaloDiasType = ( parameter1:number, parameter2:number ) => string

Crie um método chamado calculadora que receberá uma lista de argumentos + função callback que fara o cálculo desejado.

type calculadoraType = ( callback:somaType, ...parameters:number[] ) => number
*/
