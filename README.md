## Por que posso ser um bom programador ?

- Tenho conhecimento de banco de dados e sql.
- Gosto de resolver problemas lógicos.
- Tenho um pouco de conhecimento na área e uma breve experiência com Java.
- Gosto, quando possível, de criar processos automáticos para tarefas comuns.
- Procuro aprender por conta, sempre que posso.
- Sei contornar as limitações da linguagem, pelo menos no sql.
- Gosto de filmes de heróis, Star Wars, computadores e outras coisa que a 
  espécie programador gosta.
