// Cria lista
var lista = []

lista.push( 0 );
lista.push( 1 );
lista.push( 2 );
lista.push( 3 );
lista.push( 4 );
lista.push( 5 );
lista.push( 6 );
lista.push( 7 );
lista.push( 8 );
lista.push( 9 );
lista.push( 10 );

// Usando o comando Array filter filtre os numeros menores de 5

function menorQue5(value) {
    return value < 5;
}
var arow = value => value < 5

var lista2 = lista.filter(value => value < 5);
console.log( "Lista1: ",lista );
console.log( "Lista menores que 5: ",lista2 );

// Usando o comando Array map multiplique a lista por 3

function multiplicaPor3(value) {
    return value * 3;
  }
var lista2 = lista.map( multiplicaPor3 );
console.log( "Lista x 3: ",lista2 );

// Usando o comando Array find localize o numero5 na lista

function igual5(value) {
    if ( value != 5 ) {
        return false;
    }
    return value;
}
var cinco = lista.find( igual5 );
console.log( "Mostra 5: ",cinco );
