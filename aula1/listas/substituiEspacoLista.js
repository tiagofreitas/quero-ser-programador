function SubstituiEspaco( teste ) {
	return teste.replace(/ /g, "-");
}

function SubstituiEspacoLista( lista ) {
	var lista2 = [];
		for ( var texto of lista ) {
		lista2.push( SubstituiEspaco( texto ) );
	}
	return lista2;
}
var lista = ["Meu texto","Meu texto teste","Meu texto teste  1"," Meu texto teste  1"]
console.log( "Substitui espaços no texto 'Meu texto, Meu texto teste, Meu texto teste  1, Meu texto teste  1'\n Resultado: ",SubstituiEspacoLista( lista ) );
