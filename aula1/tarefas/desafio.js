
console.log("Desafio");
function removeItens(lista, exclusoes ) {
    for (var i =0; i<lista.length; i++ ) {
        if ( exclusoes.indexOf(lista[i] ) > -1 ) {
            lista.splice(i,1)
        }
    }
    console.log(lista)
}
removeItens([ 1,2,3,4,5 ],[1,2])
removeItens([ 1,2,3,4,5 ],[1,3])
removeItens([ 1,2,3,4,5 ],[3,4])

// Correção

function removeItensCorreto(lista, exclusoes ) {
    for (var i =0; i<lista.length; i++ ) {
        if ( exclusoes.indexOf(lista[i] ) > -1 ) {
            lista.splice(i,1);
            i = i-1;
        }
    }
    console.log(lista)
}

console.log('\n',"Correção");

removeItensCorreto([ 1,2,3,4,5 ],[1,2])
removeItensCorreto([ 1,2,3,4,5 ],[1,3])
removeItensCorreto([ 1,2,3,4,5 ],[3,4])
removeItensCorreto([ 2,2,3,2,5 ],[3,2])
removeItensCorreto([ 1,3,3,3,5 ],[3,2])

// Usando o comando filter

function removeItensFilter(lista, exclusoes) {
    function valido(numero) {
        for (var i =0; i<exclusoes.length; i++ ) {
            if ( numero === exclusoes[i] ) {
                return false;
            }
        }
        return true;
    }

    var lista2 = lista.filter(valido);
    console.log( lista2 );
}

console.log('\n',"Filter");

removeItensFilter([ 1,2,3,4,5 ],[1,2])
removeItensFilter([ 1,2,3,4,5 ],[1,3])
removeItensFilter([ 1,2,3,4,5 ],[3,4])
removeItensFilter([ 2,2,3,2,5 ],[3,2])
removeItensFilter([ 1,3,3,3,5 ],[3,2])
