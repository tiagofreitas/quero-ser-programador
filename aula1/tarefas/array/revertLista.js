function reverteString( texto ) {
    return texto.split("").reverse().join("");
}

function reverteLista( lista ) {
	var lista2 = [];
		for ( var texto of lista ) {
		lista2.push( reverteString( texto ) );
	}
	return lista2;
}
var lista = ["Acalasia","Catalepsia patológica","Epilepsia","Síndrome de Tourette","Doença de Huntington","Síndroma de Shy-Drager","Bulimia","Escorbuto","Gota"]
console.log( reverteLista( lista ) );
