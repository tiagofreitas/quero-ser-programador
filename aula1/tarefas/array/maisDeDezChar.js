
//Crie uma lista de textos e filtre os que tem mais de 10 caracteres
function maiorQue10(texto) {
    return texto.length > 10;
}

var lista = ["Acalasia","Catalepsia patológica","Epilepsia","Síndrome de Tourette","Doença de Huntington","Síndroma de Shy-Drager","Bulimia","Escorbuto","Gota"];
var lista2 = lista.filter(maiorQue10);
console.log( lista2 );
