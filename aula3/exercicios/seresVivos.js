"use strict";
exports.__esModule = true;
var Planta = /** @class */ (function () {
    function Planta() {
        this.tipo = "Vegetal";
        this.listaAlimentos = ["Organico"];
    }
    Planta.prototype.alimentar = function (alimento) {
        if (this.listaAlimentos.indexOf(alimento.tipo) > -1) {
            console.log("O alimento foi consumido");
        }
        else {
            throw "Isto não é alimento de vegetal";
        }
    };
    return Planta;
}());
exports.Planta = Planta;
var Gafanhoto = /** @class */ (function () {
    function Gafanhoto() {
        this.tipo = "Inseto";
        this.listaAlimentos = ["Vegetal"];
    }
    Gafanhoto.prototype.alimentar = function (alimento) {
        if (this.listaAlimentos.indexOf(alimento.tipo) > -1) {
            console.log("O " + alimento.tipo + " foi consumido");
        }
        else {
            throw "Isto não é alimento de inseto";
        }
    };
    return Gafanhoto;
}());
exports.Gafanhoto = Gafanhoto;
var Sapo = /** @class */ (function () {
    function Sapo() {
        this.tipo = "Anfibio";
        this.listaAlimentos = ["Inseto"];
    }
    Sapo.prototype.alimentar = function (alimento) {
        if (this.listaAlimentos.indexOf(alimento.tipo) > -1) {
            console.log("O alimento foi consumido");
        }
        else {
            throw "Isto não é alimento de anfíbio";
        }
    };
    return Sapo;
}());
exports.Sapo = Sapo;
var Fungos = /** @class */ (function () {
    function Fungos() {
        this.tipo = "Decompositores";
        this.listaAlimentos = ["Vegetal", "Inseto", "Anfibio"];
    }
    Fungos.prototype.alimentar = function (alimento) {
        if (this.listaAlimentos.indexOf(alimento.tipo) > -1) {
            console.log("O alimento foi consumido");
        }
        else {
            throw "Isto não é alimento de decompositores";
        }
    };
    return Fungos;
}());
exports.Fungos = Fungos;
