export interface Organico {
    tipo:string
    alimentar : ( Organico ) => void;
}
export interface Vegetal {
    listaAlimentos:string[]
}
export interface Inseto {
    listaAlimentos:string[]
}
export interface Anfibio {
    listaAlimentos:string[]
}
export interface Decompositores {
    listaAlimentos:string[]
}
