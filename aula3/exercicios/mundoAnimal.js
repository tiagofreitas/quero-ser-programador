"use strict";
exports.__esModule = true;
var seresVivos_1 = require("./seresVivos");
var planta = new seresVivos_1.Planta;
var gafanhoto = new seresVivos_1.Gafanhoto;
var sapo = new seresVivos_1.Sapo;
var fungos = new seresVivos_1.Fungos;
var consumir = function (consumidor, produtor) {
    try {
        console.log(consumidor.tipo, "tenta se alimentar de", produtor.tipo + ": ");
        consumidor.alimentar(produtor);
    }
    catch (e) {
        console.log(e);
    }
};
consumir(planta, planta);
consumir(planta, gafanhoto);
consumir(gafanhoto, planta);
consumir(sapo, gafanhoto);
consumir(sapo, planta);
consumir(fungos, gafanhoto);
consumir(fungos, planta);
consumir(fungos, sapo);
