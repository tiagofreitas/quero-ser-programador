import { Organico,Vegetal,Inseto,Anfibio,Decompositores } from './cadeiaAlimentar';

export class Planta implements Vegetal,Organico {
    tipo = "Vegetal"
    listaAlimentos = ["Organico"]
    alimentar ( alimento:Organico ) {
        if ( this.listaAlimentos.indexOf( alimento.tipo ) > -1 ) {
            console.log( "O alimento foi consumido")
        } else {
            throw "Isto não é alimento de vegetal"
        }
    }
}

export class Gafanhoto implements Inseto,Organico {
    tipo = "Inseto"
    listaAlimentos = ["Vegetal"]
    alimentar ( alimento:Organico ) {
        if ( this.listaAlimentos.indexOf( alimento.tipo ) > -1 ) {
            console.log( `O ${ alimento.tipo } foi consumido`)
        } else {
            throw "Isto não é alimento de inseto"
        }
    }
}

export class Sapo implements Anfibio,Organico {
    tipo = "Anfibio"
    listaAlimentos = ["Inseto"]
    alimentar ( alimento:Organico ) {
        if ( this.listaAlimentos.indexOf( alimento.tipo ) > -1 ) {
            console.log( "O alimento foi consumido")
        } else {
            throw "Isto não é alimento de anfíbio"
        }
    }
}

export class Fungos implements Decompositores,Organico {
    tipo = "Decompositores"
    listaAlimentos = ["Vegetal","Inseto","Anfibio"]
    alimentar ( alimento:Organico ) {
        if ( this.listaAlimentos.indexOf( alimento.tipo ) > -1 ) {
            console.log( "O alimento foi consumido")
        } else {
            throw "Isto não é alimento de decompositores"
        }
    }
}
