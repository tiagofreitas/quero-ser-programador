import { Organico,Vegetal,Inseto,Anfibio,Decompositores } from './cadeiaAlimentar';
import { Planta,Gafanhoto,Sapo,Fungos } from './seresVivos';

var planta = new Planta;
var gafanhoto = new Gafanhoto;
var sapo = new Sapo;
var fungos = new Fungos;

let consumir = ( consumidor:Organico, produtor:Organico ) => {
    try {
        console.log( consumidor.tipo, "tenta se alimentar de",`${ produtor.tipo }: ` );
        consumidor.alimentar( produtor );
    } catch(e) {
        console.log(e); }
}

consumir( planta, planta );
consumir( planta, gafanhoto );
consumir( gafanhoto, planta );
consumir( sapo, gafanhoto );
consumir( sapo, planta );
consumir( fungos, gafanhoto );
consumir( fungos, planta );
consumir( fungos, sapo );
