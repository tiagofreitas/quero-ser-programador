import { Assento } from './interface'
export class Sofa implements Assento {
    ocupado1 = false
    ocupado2 = false
    sentar () {
        if ( this.ocupado1 != true ) {
            this.ocupado1 = true
            console.log( "Sentou no sofa" )
        } else if (  this.ocupado2 != true  ) {
            this.ocupado2 = true
            console.log( "Sentou no sofa" )
        } else {
            throw "Não tem lugar"
        }
    }
}

var sofa:Assento = new Sofa;

for (var i = 0;i < 3;i++) {
    try {
        sofa.sentar();
        }
    catch(e) {
        console.log(e);
    }
}
