"use strict";
exports.__esModule = true;
var Cadeira = /** @class */ (function () {
    function Cadeira() {
        this.ocupado = false;
    }
    Cadeira.prototype.sentar = function () {
        if (this.ocupado) {
            throw "Não tem lugar";
        }
        else {
            this.ocupado = true;
            console.log("Sentou na cadeira");
        }
    };
    return Cadeira;
}());
var cadeira1 = new Cadeira;
cadeira1.sentar();
try {
    cadeira1.sentar();
}
catch (e) {
    console.log(e);
}
