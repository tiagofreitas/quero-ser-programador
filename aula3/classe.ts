import { Assento } from './interface'
class Cadeira implements Assento {
    ocupado = false
    sentar () {
        if ( this.ocupado ) {
            throw "Não tem lugar"
        } else {
            this.ocupado = true
            console.log( "Sentou na cadeira" )
        }
    }
}

var cadeira1 = new Cadeira;
cadeira1.sentar();
try {
    cadeira1.sentar();
    }
  catch(e) {
    console.log(e);
}
