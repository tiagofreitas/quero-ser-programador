"use strict";
exports.__esModule = true;
var Sofa = /** @class */ (function () {
    function Sofa() {
        this.ocupado1 = false;
        this.ocupado2 = false;
    }
    Sofa.prototype.sentar = function () {
        if (this.ocupado1 != true) {
            this.ocupado1 = true;
            console.log("Sentou no sofa");
        }
        else if (this.ocupado2 != true) {
            this.ocupado2 = true;
            console.log("Sentou no sofa");
        }
        else {
            throw "Não tem lugar";
        }
    };
    return Sofa;
}());
exports.Sofa = Sofa;
var sofa = new Sofa;
for (var i = 0; i < 3; i++) {
    try {
        sofa.sentar();
    }
    catch (e) {
        console.log(e);
    }
}
